import App from './App'
import store from './store'
// #ifndef VUE3
import Vue from 'vue'
import uView from '@/uni_modules/uview-ui'
Vue.use(uView)
import self from '@/utils/nativeRtc'
import bookData from '@/mock/data'
Vue.prototype.$nativeRtc = self;
Vue.prototype.$store = store; 
Vue.prototype.$bookData = bookData; 
Vue.config.productionTip = false
App.mpType = 'app'
const app = new Vue({
    ...App
})
app.$mount()
// #endif

// #ifdef VUE3
import { createSSRApp } from 'vue'
export function createApp() {
  const app = createSSRApp(App)
  return {
    app
  }
}
// #endif