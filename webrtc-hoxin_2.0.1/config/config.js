module.exports = {
  // baseUrl: 'http://47.93.131.152:9099',
  baseUrl: 'http://39.99.150.41:9099',
  cim_host: 'ws://39.99.150.41:45678',
  avatarUrl: 'https://gbreview.oss-cn-beijing.aliyuncs.com',
  iceServers: [{
  		"uri": "turn:turn.farsunset.com:3478",
  		"password": "coturnv5",
  		"username": "coturn"
  	},
  	{
  		"uri": "stun:stun.l.google.com:19302"
  	}
  ],
}
