import request, {
  Method
} from '@/utils/request.js'
import md5 from '@/utils/js-md5.js'
// 登录
export function login(params) {
  params.telephone = '+86' + params.telephone
  params.password = md5(params.password)
  return request.ajax({
    url: `/user/login`,
    method: 'post',
    needToken: false,
    isJson: false,
    params
  })
}
export function logout() {
  return request.ajax({
    url: '/user/logout',
    method: 'get',
    needToken: true,
  })
}