import request, {
  Method
} from '@/utils/request.js'

// 发送语音通话请求
export function callVoice(data) {
  return request.ajax({
    url: `/webrtc/voice`,
    method: 'POST',
    needToken: true,
    data
  })
}
// 发送群语音通话请求
export function callVoices(data) {
  return request.ajax({
    url: `/webrtc/group/voice`,
    method: 'POST',
    needToken: true,
    isJson: true,
    data
  })
}
// 发送视频通话请求
export function callVideo(data) {
  return request.ajax({
    url: `/webrtc/video`,
    method: 'POST',
    needToken: true,
    data
  })
}
// 同意请求
export function acceptCall(data) {
  return request.ajax({
    url: `/webrtc/accept`,
    method: 'POST',
    needToken: true,
    data
  })
}

// 同步ice
export function transmitice(data) {
  return request.ajax({
    url: `/webrtc/transmit/ice`,
    method: 'POST',
    needToken: true,
    data,
    isJson: true,
  })
}
// 发送offer
export function transmitoffer(data) {
  return request.ajax({
    url: `/webrtc/transmit/offer`,
    method: 'POST',
    needToken: true,
    data,
    isJson: true,
  })
}
// 发送answer
export function transmitanswer(data) {
  return request.ajax({
    url: `/webrtc/transmit/answer`,
    method: 'POST',
    needToken: true,
    data,
    isJson: true,
  })
}

// 挂断
export function hangup(data) {
  return request.ajax({
    url: `/webrtc/hangup`,
    method: 'POST',
    needToken: true,
    data
  })
}
// 拒绝
export function rejectCall(data) {
  return request.ajax({
    url: `/webrtc/reject`,
    method: 'POST',
    needToken: true,
    data
  })
}

// 忙线请求
export function busyCall(data) {
  return request.ajax({
    url: `/webrtc/busy`,
    method: 'POST',
    needToken: true,
    data
  })
}

// 取消呼叫
export function cancelCall(data) {
  return request.ajax({
    url: `/webrtc/cancel`,
    method: 'POST',
    needToken: true,
    data
  })
}


// 群发送answer
export function transmitanswers(data) {
  return request.ajax({
    url: `/webrtc/group/transmit/answer`,
    method: 'POST',
    needToken: true,
    data,
    isJson: true,
  })
}
// 群发送offer
export function transmitoffers(data) {
  return request.ajax({
    url: `/webrtc/group/transmit/offer`,
    method: 'POST',
    needToken: true,
    data,
    isJson: true,
  })
}
// 群同步ice
export function transmitices(data) {
  return request.ajax({
    url: `/webrtc/group/transmit/ice`,
    method: 'POST',
    needToken: true,
    data,
    isJson: true,
  })
}
// 群取消呼叫
export function cancelCalls(data) {
  return request.ajax({
    url: `/webrtc/group/cancel`,
    method: 'POST',
    needToken: true,
    data
  })
}

// 群忙线请求
export function busyCalls(data) {
  return request.ajax({
    url: `/webrtc/group/busy`,
    method: 'POST',
    needToken: true,
    data
  })
}
// 群拒绝
export function rejectCalls(data) {
  return request.ajax({
    url: 'webrtc/group/reject',
    method: 'POST',
    needToken: true,
    data
  })
}
 
// 退出群通话
export function quitCalls(data) {
  return request.ajax({
   url: `/webrtc/group/quit`,
    method: 'POST',
    needToken: true,
    data
  })
}
// 群重新呼叫
export function resetCalls(data) {
  return request.ajax({
    url: 'webrtc/group/recall',
    method: 'POST',
    needToken: true,
    data
  })
}

// 完成解散会议
export function finishCalls(data) {
  return request.ajax({
    url: `/webrtc/group/finish`,
    method: 'POST',
    needToken: true,
    data
  })
}
// 同意请求
export function acceptCalls(data) {
  return request.ajax({
    url: `/webrtc/group/accept`,
    method: 'POST',
    needToken: true,
    isJson: true,
    data
  })
}

// 超时
export function timeoutCalls(data) {
  return request.ajax({
    url: `/webrtc/group/timeout`,
    method: 'POST',
    needToken: true,
    data
  })
}

// 发送群视频通话请求
export function callVideos(data) {
  return request.ajax({
    url: `/webrtc/group/video`,
    method: 'POST',
    needToken: true,
    isJson: true,
    data
  })
}
// 摄像头切换
export function cameraSwitch(data) {
  return request.ajax({
    url: `/webrtc/group/camera/switch`,
    method: 'POST',
    needToken: true,
    // isJson: true,
    data
  })
}
// 音频切换
export function voiceSwitch(data) {
  return request.ajax({
    url: `/webrtc/group/microphone/switch`,
    method: 'POST',
    needToken: true,
    // isJson: true,
    data
  })
}
// 全员禁言
export function voiceDisable(data) {
  return request.ajax({
    url: `/webrtc/group/microphone/disable`,
    method: 'POST',
    needToken: true,
    // isJson: true,
    data
  })
}
// 取消全员禁言
export function voiceResume(data) {
  return request.ajax({
    url: `/webrtc/group/microphone/resume`,
    method: 'POST',
    needToken: true,
    // isJson: true,
    data
  })
}
// 邀请成员
export function groupAdd(data) {
  return request.ajax({
    url: `/webrtc/group/add`,
    method: 'POST',
    needToken: true,
    isJson: true,
    data
  })
}
// 邀请成员
export function groupRecall(data) {
  return request.ajax({
    url: `/webrtc/group/recall`,
    method: 'POST',
    needToken: true,
    isJson: true,
    data
  })
}
