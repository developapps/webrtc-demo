import {
  login,
  logout
} from '@/api/login'

import {
  getToken,
  setToken,
  removeToken,
  getUserInfo,
  setUserInfo,
  removeUserInfo
} from '@/utils/auth'
import Cache, {
  Keys
} from '@/utils/cache'
var _ = require('lodash')
const state = {
  entering: false,
  socket: false,
  token: getToken(),
  user: getUserInfo(),
  baseInfo: {},
  replyList: [],
  emojList: [],
  fzIcon: 0,
  fzReply: [],
  isUpdate: false,
  callId: -1, //当前通话人id
  applyFriend: [],
  symbolGroup: [],
  shieldGroup: [], // 被屏蔽群消息的id
  onLine: false,
}
let userInfo = state.user
const mutations = {
  SET_USER(state, data) {
    setUserInfo(data)
    userInfo = data
    state.user = data
  },
  SET_TOKEN(state, data) {
    setToken(data)
    console.log(data)
    state.token = data
  },
  SET_BASEINFO: (state, baseInfo) => {
    // 设置基本信息
    const clonedata = _.cloneDeep(baseInfo)
    Cache.setItem(Keys.baseInfo, [clonedata])
    state.baseInfo = clonedata
  },
  SET_ONLINE(state, onLine) {
    state.onLine = onLine
  },
  SET_CALLID(state, callId) {
    // console.log('================打印callId================')
    state.callId = callId
  },
  CLEAR_VUEX(state) {
    // 退出登录情况vuex
    state.replyList = []
    state.baseInfo = {}
    state.fzReply = []
    state.emojList = []
    state.applyFriend = []
    state.symbolGroup = []
    state.fzIcon = 0
    state.isUpdate = false
  }
}
const actions = {
  // user login
  login({
    commit
  }, userInfo) {
    const {
      telephone,
      password
    } = userInfo
    return new Promise((resolve, reject) => {
      login({
          telephone: telephone.trim(),
          password: password
        })
        .then(async response => {
          if (response.code === 200) {
            commit('SET_TOKEN', response.token)
            setToken(response.token)
            commit('SET_USER', response.data)
            resolve(response.data)
          } else {
            reject(response)
          }
        })
        .catch(error => {
          reject(error)
        })
    })
  },

  
  // 创建回话列表
  replyList({
    commit
  }, data) {
    return new Promise(resolve => {
      commit('SET_REPLYLIST', data)
      resolve()
    })
  },
  // user logout
  logout({
    commit,
    state
  }) {
    return new Promise((resolve, reject) => {
      logout(state.token)
        .then(async () => {
          let isItem = await Cache.getItem(`symbolGroup${userInfo.id}`)
          if (!isItem || isItem.length == 0) {
            Cache.removeItem(`symbolGroup${userInfo.id}`)
          }
          commit('SET_TOKEN', '')
          commit('SET_USER', '')
          commit('SET_BASEINFO', {})
          commit('CLEAR_VUEX')
          // commit('SET_EMOJLIST', '')
          Cache.removeItem(Keys.baseInfo)
          Cache.removeItem('fzList')
          Cache.removeItem('emojList')
          Cache.removeItem('fz')
          removeToken()
          removeUserInfo()
          resolve()
          // 检测 属于自己的symbolGroup+id 是否还有内容 没有内容就移除\
        })
        .catch(error => {
          reject(error)
        })
    })
  },

  // remove token
  resetToken({
    commit
  }) {
    return new Promise(async (resolve) => {
      const isItem = await Cache.getItem(`symbolGroup${userInfo.id}`)
      if (!isItem || isItem.length == 0) {
        Cache.removeItem(`symbolGroup${userInfo.id}`)
      }
      commit('SET_TOKEN', '')
      commit('SET_USER', '')
      commit('SET_BASEINFO', {})
      commit('CLEAR_VUEX')
      commit('SET_EMOJLIST', '')
      Cache.removeItem(Keys.baseInfo)
      Cache.removeItem('fzList')
      Cache.removeItem('emojList')
      Cache.removeItem('fz')
      removeToken()
      removeUserInfo()
      resolve()
    })
  }
}

export default {
  namespaced: true,
  state,
  mutations,
  actions
}
