import Cache, { Keys } from "@/utils/cache";
const state = {
  skin: '',
  isWinMaximize: false,
  isSideCollapsed: false,
  winType: ""
};

const mutations = {
  SET_WINMAXIMIZE(state, data) {
    Cache.setItem(Keys.isWinMaximize, data);
    state.isWinMaximize = data;
  },
  SET_SKIN(state, data) {
    Cache.setItem(Keys.skin, data);
    state.skin = data;
  },
  SET_SIDECOLLAPSED(state, data) {
    Cache.setItem(Keys.isSideCollapsed, data);
    state.isSideCollapsed = data;
  }
};

const actions = {};

export default {
  namespaced: true,
  state,
  mutations,
  actions
};
