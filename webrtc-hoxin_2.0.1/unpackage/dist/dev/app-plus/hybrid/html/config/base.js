const baseconfig = {
  // baseUrl: 'http://47.93.131.152:9099',
  // 如果浏览器端H5使用音视频通话的业务，必须要使用https协议
  baseUrl: 'http://39.99.150.41:9099',
  avatarUrl: 'https://gbreview.oss-cn-beijing.aliyuncs.com',
  // 修改为自建信令服务器信息
  iceServer: 'turn:turn.farsunset.com:3478',
  username: 'coturn',
  credential: 'coturnv5'
}