(function(doc, win) {
  var docEl = doc.documentElement,
    isIOS = navigator.userAgent.match(/(iPhone|iPod|Android|ios)/i),
    dpr = isIOS ? Math.min(win.devicePixelRatio, 3) : 1,
    dpr = window.top === window.self ? dpr : 1, //被iframe引用时，禁止缩放
    dpr = 1,
    scale = 1 / dpr,
    resizeEvt = 'orientationchange' in window ? 'orientationchange' : 'resize';
  docEl.dataset.dpr = dpr;
  var metaEl = doc.createElement('meta');
  metaEl.name = 'viewport';
  metaEl.content = 'initial-scale=' + scale + ',maximum-scale=' + scale + ', minimum-scale=' + scale;
  docEl.firstElementChild.appendChild(metaEl);
  var recalc = function() {
    var width = docEl.clientWidth;
    if (width / dpr > 750) {
      width = 750 * dpr;
    }
    // 乘以100，px : rem = 100 : 1
    docEl.style.fontSize = 100 * (width / 750) + 'px';
  };
  recalc()
  if (!doc.addEventListener) return;
  win.addEventListener(resizeEvt, recalc, false);
})(document, window);

function getQueryVariable(variable) {
  var query = window.location.search.substring(1);
  var vars = query.split("&");
  for (var i = 0; i < vars.length; i++) {
    var pair = vars[i].split("=");
    if (pair[0] == variable) {
      return pair[1];
    }
  }
  return (false);
}

let removeAllClass = (className, removeClass) => {
  //找到所有包含className的结点
  let nodeList = document.querySelectorAll(`.${className}`);
  //删除这些标签对应的class
  Array.prototype.forEach.call(nodeList, (el) => {
    el.classList.remove(removeClass)
  })
}
let addAllClass = (className, addClass) => {
  //找到所有包含className的结点
  let nodeList = document.querySelectorAll(`.${className}`);
  //删除这些标签对应的class
  Array.prototype.forEach.call(nodeList, (el) => {
    el.classList.add(addClass)
  })
}
let timer = null
let count = 0
let timeString = ''
let startTime = function(dom) {
  timer = setInterval(() => {
    count++;
    dom.innerHTML = showNum(parseInt(count / 60 / 60)) + ':' + showNum(parseInt(count / 60) % 60) + ':' + showNum(
      count % 60)
    timeString = showNum(parseInt(count / 60 / 60)) + ':' + showNum(parseInt(count / 60) % 60) + ':' + showNum(
      count % 60)
    uniRtc.setCallTime(timeString)
  }, 1000)
}

function showNum(num) {
  if (num < 10) {
    return '0' + num
  }
  return num
}
const updateInfo = function() { // 更新通话人信息
  const elavatar = document.getElementById('avatar')
  const url = baseconfig.avatarUrl  + '/' + getQueryVariable('id') + '.jpg'
  elavatar.style.background = `url(${url}) no-repeat center center`
  elavatar.style.backgroundSize = 'cover'
  const elname = document.getElementById('name')
  elname.innerHTML = decodeURIComponent(getQueryVariable('name'))
}

function acceptVoice() {
  uniRtc.postMsg({
    acceptVoice: 'acceptVoice',
  }, '*')
}

function acceptVideo() {
  uniRtc.postMsg({
    acceptVideo: 'acceptVideo',
  }, '*');
}

function cancelBtn() {
  uniRtc.timeString = timeString
  uniRtc.cancelBtn()
}

function cancelBtnd() {
  uniRtc.timeString = timeString
  uniRtc.cancelBtnd()
}

function videoBtn() {
  uniRtc.videoBtn((platform, headset) => {
    if (headset) {
      removeAllClass('voiceBtnIn', 'bg-white')
      addAllClass('voiceBtnIn', 'line-white')
      addAllClass('voiceBtnIn', 'close')
    } else {
      removeAllClass('voiceBtnIn', 'line-white')
      removeAllClass('voiceBtnIn', 'close')
      addAllClass('voiceBtnIn', 'bg-white')
    }
  })
}

function voiceBtn() {
  uniRtc.voiceBtn((platform, headset) => {
    if (headset) { //打开 => 关闭
      removeAllClass('voiceBtnIn', 'bg-white')
      addAllClass('voiceBtnIn', 'line-white')
      addAllClass('voiceBtnIn', 'close')
    } else {
      removeAllClass('voiceBtnIn', 'line-white')
      removeAllClass('voiceBtnIn', 'close')
      addAllClass('voiceBtnIn', 'bg-white')
    }
  })
}

function speakBtn() {
  uniRtc.speakBtn((platform, horn) => {
    const speakBtnText = document.getElementById('speakBtnText')
    if (horn) { //扬声器 => 听筒
      removeAllClass('speakBtnIn', 'bg-white')
      addAllClass('speakBtnIn', 'line-white')
      if (platform === 'h5') {
        speakBtnText.innerHTML = '静音'
        addAllClass('speakBtnIn', 'close')
      }
      if (platform === 'app') {
        speakBtnText.innerHTML = '听筒'
      }
    } else { //听筒 => 扬声器
      removeAllClass('speakBtnIn', 'line-white')
      addAllClass('speakBtnIn', 'bg-white')
      if (platform === 'h5') {
        removeAllClass('speakBtnIn', 'close')
        speakBtnText.innerHTML = '听筒'
      }
      if (platform === 'app') {
        speakBtnText.innerHTML = '扬声器'
      }
    }
  })
}

function exchangeVideo() {
  uniRtc.exchangeVideo()
}

function changeMedia(){
	$('.meida-modal').addClass('show')
}
function handleCloseMedia(){
	$('.meida-modal').removeClass('show')
}


function changeVideo() {
  uniRtc.changeVideo((platform, horn) => {
    if (horn) {
      removeAllClass('speakBtnIn', 'bg-white')
      addAllClass('speakBtnIn', 'line-white')
    } else {
      removeAllClass('speakBtnIn', 'line-white')
      addAllClass('speakBtnIn', 'bg-white')
    }
  })
}
// 开始共享桌面
function handleDeskShare(){
	
	console.log('开始共享桌面',)
	uniRtc.handleDeskShare()
}
