import Vue from 'vue'
let vm = new Vue();
import store from '@/store/index'
import configs from '@/config/config'
import bookData from '@/mock/data'
import {
	webSocket
} from '@/utils/webSocket' // 开启webSocket
var core = {
	navigation: {
		popup: []
	},
	//#ifdef H5 
	platform: 'h5',
	//#endif
	//#ifdef APP-PLUS
	platform: 'app',
	//#endif
	version: '1.1.1',
	loading: false,
	appshowing: true,
	inited: false,
	debug: false,
	jweixin: null,
	device: uni.getSystemInfoSync().platform,
	Client: {
		screenHeight: 0,
		screenWidth: 0,
		windowHeight: 0,
		statusBarHeight: 0
	},
	system: {
		name: 'hoxin',
		logo: '../../static/logo.png',
		siteroot: '',
		splash: '../../static/splash.jpg',
		defaultimg: "",
		wallpaper: '/static/wallpaper.png',
		apprunshow: [],
		call: '/static/music/call.mp3',
		close: '/static/music/close.wav',
		msg: '/static/music/msg.mp3',
		camera: '/static/music/camera.wav',
		auditmode: {
			"status": false
		}
	},
	style: {
		navbg: 'bg-white',
		actcolor: 'limegreen',
		link: '#0081ff',
		chatbg: '#96d9dc',
		active: '#04be02'
	},
	langs: {
		text_back: '返回',
		text_contentover: '松开手开始加载',
		text_contentdown: '下拉加载更早的信息',
		text_confirm_title: '温馨提示',
		text_confirm_delete: "确定要删除吗？",
		text_delete_confirm: '删除后不可恢复，是否确定要删除？',
		text_friends_album: "朋友圈",
		text_groupname: '群组',
		text_empty: '空空如也',
		text_please_input_remark: '请填写备注信息',
		text_alert_app_action: "请在APP上使用此功能",
		text_dialog_delete: "您可以通过联系人重新发起会话",
		msg_no_anymore: "没有更多了~"
	},
	init: async function(userstate = '') {
		let uid = uni.getStorageSync('x-uid');
		if (!uid) {
			uni.reLaunch({
				url: "/pages/auth/index"
			})
			return
		} else {	
		}
		webSocket()
		this.initNativeRtc()
	},
	async initNativeRtc() {
		const userArr = bookData
		vm.$nativeRtc.isUIKitEnable() && vm.$nativeRtc.setup()
		let nativeFri = []
		userArr.forEach(item => {
			if (store.getters.user.id != item.id) {
				const obj = {
					"id": item.id,
					"name": item.alias || item.name,
					"logo": configs.avatarUrl + '/' + item.id + '.jpg'
				}
				nativeFri.push(obj)
			}

		})
		vm.$nativeRtc.isUIKitEnable() && vm.$nativeRtc.updateFriendList({
			friends: nativeFri
		})
	},
	confirm: function(msg, title, callback, options = {}, Cancel) {
		options = options || {};
		options.content = msg;
		if (title) options.title = title;
		if (typeof(options.success) == 'undefined' && typeof(callback) == 'function') {
			options.success = function(res) {
				if (res.confirm) {
					callback();
				} else if (res.cancel && typeof(Cancel) == 'function') {
					Cancel();
				}
			};
		}
		return uni.showModal(options);
	},
	navito: function(page = '', data = {}, type = 0) {
		if (page.indexOf('tel:') == 0) {
			return uni.makePhoneCall({
				phoneNumber: page.replace('tel:', '')
			});
		}
		var url = this.page(page, data);
		if (url == '') return false;
		if (this.inwechat) this.jweixin = null;
		switch (type) {
			case 0:
				uni.navigateTo({
					url,
					fail: console.log
				})
				break;
			case 1:
				uni.redirectTo({
					url,
					fail: console.log
				})
				break;
			case 2:
				uni.reLaunch({
					url,
					fail: console.log
				})
				break;
			case 3:
				uni.navigateBack({
					delta: url,
					fail: console.log
				})
				break;
			case 4:
				uni.switchTab({
					url: url,
					fail: console.log
				})
				break;
		}
	},
	page: function(url = 'index/index', data = {}) {
		if (url == '' || url == '#' || url.indexOf('javascript:') == 0) return '';
		// ***#/pages/***
		if (url.indexOf('#/pages/') > 0) {
			let urls = url.split('#/pages/');
			url = urls[1];
		} else {
			// *** /#/pages/ ***
			url = url.replace('#/pages/', '', url);
		}
		for (let key in data) {
			if (url.indexOf('http') == 0) {
				url += '&' + key + '=' + data[key]
			} else {
				url += (url.indexOf('?') == -1 ? '?' : '&') + key + '=' + data[key]
			}

		}
		// console.log(url)
		// console.log(encodeURIComponent(url))
		if (url.indexOf('http') == 0) {
			url = '/pages/web/index?url=' + url
		}
		if (url.indexOf('/pages/') != 0) url = '/pages/' + url;
		console.log(url)
		return url;
	},
	back: function() {
		if (this.inwechat) this.jweixin = null;
		let pages = getCurrentPages();
		if (pages.length > 1) {
			uni.navigateBack();
		} else {
			uni.reLaunch({
				url: '/pages/index/index'
			})
		}
	},
	report: function(res, initpage = false) {
		if (res === null) res = {
			message: "敬请期待",
			redirect: 'back',
			type: "error"
		};
		if (typeof(res) != 'object' && this.isJsonString(res)) {
			res = JSON.parse(res);
		}
		if (typeof(res) != 'object') return false;
		if (res.redirect == '' && initpage) {
			res.redirect = 'back';
		}
		if (typeof(res.url) != 'undefined') {
			var act = typeof(res.act) == 'undefined' ? 'undefined' : res.act;
			if (act == 'redirect') {
				this.navito(res.url, 1);
			}
		}
		if (typeof(res.message) != 'undefined') {
			this.toast(res.message, res.redirect, res.type);
		}
	},
	toast: function(m, u, t) {
		var toastinfo = {
			title: m,
			duration: 1500
		};
		if (this.loading) {
			uni.hideLoading();
			this.loading = false;
		}
		var that = this;
		if (u) {
			let redirect = function() {
				if (u == 'back') {
					that.back();
				} else if (u == 'home') {
					that.navito('index/index', {}, 2);
				} else if (u == 'login') {
					that.navito('auth/index', {
						act: 'login'
					}, 1);
				} else {
					that.navito(u, {}, 1);
				}
			};
			if (t == 'redirect') {
				return redirect();
			}
			toastinfo.complete = function() {
				setTimeout(redirect, 800);
			}
		}
		toastinfo.icon = (t == 'success' || t == 'loading') ? t : 'none';
		uni.showToast(toastinfo);
	},
	isJsonString: function(str) {
		try {
			if (typeof(JSON.parse(str)) == "object") {
				return true;
			}
		} catch (e) {}
		return false;
	},
	notopen: function(str) {
		console.log('当前版本不支持' + str + '，请联系作者购买完整版');
		this.toast('暂未开放');
	},
	copy: function(text = '', toast = '已复制到剪切板') {
		if (text == '') return this.toast('复制的内容为空');
		// text =  this.replaceEmoj(text)
		console.log(text)
		return uni.setClipboardData({
			data: text,
			success: function() {
				core.toast(toast, '', 'success');
			},
			fail: function(e) {
				console.log(e);
				core.toast('复制失败');
			}
		});
		return
		if (this.platform == 'h5') {
			let result
			let textarea = document.createElement("textarea")
			textarea.value = text
			textarea.readOnly = "readOnly"
			document.body.appendChild(textarea)
			textarea.select() // 选中文本内容
			textarea.setSelectionRange(0, text.length)
			// uni.showToast({//提示
			// 	title:'复制成功' 
			// })
			result = document.execCommand("copy")
			textarea.remove()
			core.toast(toast, '', 'success');
			// return this.toast('请在APP上使用此功能');
		} else {
			return uni.setClipboardData({
				data: text,
				success: function() {
					core.toast(toast, '', 'success');
				},
				fail: function(e) {
					console.log(e);
					core.toast('复制失败');
				}
			});
		}

	},
}
module.exports = core;