//缓存的Key
export const Keys = {
  baseInfo: "baseInfo",
  replyList: "replyList",
  user: 'user',
  accessToken: 'accessToken',
  refreshToken: 'refreshToken',
  tokenExpTime: 'tokenExpTime',
};
//App缓存数据
class Cache {

  setItem(key, value) {
    try {
      if (key == Keys.accessToken) { // token 都存储到本地缓存
        uni.setStorageSync(key, value);
        if (key == Keys.accessToken) {
          uni.setStorageSync(Keys.tokenExpTime, new Date().getTime());
        }
        return false
      }
      if (key == Keys.user) { // user 都存到本地缓存
        uni.setStorageSync(key, value);
        return false
      }
     uni.setStorageSync(key, value); // app存储到本地缓存
    } catch (e) {
      console.log(e);
    }
  }

  async getItem(key) {
    if (key == Keys.accessToken || key == Keys.user) { // token 都存储到本地缓存
      return uni.getStorageSync(key)
    }
    return uni.getStorageSync(key)
  }

  removeItem(key) {
    if (key == Keys.accessToken || key == Keys.user) { // token 都存储到本地缓存
      return uni.removeStorageSync(key)
    }
    return uni.removeStorageSync(key)
   
  }

  /**
   * 登录后的会员信息
   * @return {如果返回null，则需要重新登录}
   */
  user() {
    const user = uni.getStorageSync(Keys.user);
    if (user === '' || user === null) {
      return null;
    }
    return user;
  }
};


export default new Cache();
