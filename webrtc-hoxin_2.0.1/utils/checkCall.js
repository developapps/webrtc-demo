// 通话逻辑
// 900	单人语音通话请求
// 901	单人视频通话请求
// 902	对方接听了通话
// 903	对话拒绝了通话
// 904	对方设备正忙
// 905	对方挂断结束通话
// 906	对方取消呼叫
// 907	同步webrtc的ice
// 908	收到主叫的webrtc的offer
// 909	收到被叫的webrtc的answer
import store from '@/store'
import * as API_rtc from '@/api/rtc'
import configs from '@/config/config'
import bookData from '@/mock/data'
export default function checkCall(message) {
	console.log('进入通话逻辑')
	let globalData = getApp().globalData
	let online = store.getters.onLine
	const callId = store.getters.callId
	switch (message.action) {
		case '900':
			console.log('接收到语音通话请求', online)
			if (!online) {
				store.commit('user/SET_ONLINE', true)
				const userList = bookData
				const user = userList.find(item => item.id == message.sender)
				uni.getImageInfo({
					src: `${configs.avatarUrl}/${message.sender}.jpg`,
					success: (image) => {
						uni.navigateTo({
							url: '/pages/rtc/index?type=voiced&id=' + message.sender + '&name=' +
								user.name
						})
					}
				});
			} else {
				API_rtc.busyCall({
					targetId: message.sender
				})
			}
			break;
		case '901':
			if (!online) {
				store.commit('user/SET_ONLINE', true)
				const userList = bookData
				const user = userList.find(item => item.id == message.sender)
				uni.navigateTo({
					url: '/pages/rtc/index?type=videod&id=' + message.sender + '&name=' + user.name
				})
			} else {
				API_rtc.busyCall({
					targetId: message.sender
				})
			}
			console.log('接收到视频通话请求')
			break;
		case '902':
			uni.$emit('acceptRtc', message)
			console.log('对方接听了通话');
			break;
		case '903':
			console.log('对话拒绝了通话')
			uni.$emit('rejectCall', message)
			break;
		case '904':
			console.log('对方设备正忙')
			uni.$emit('busyCall', message)
			break;
		case '905':
			console.log('对方挂断结束通话')
			uni.$emit('handleUp', message)
			break;
		case '906':
			console.log('对方取消呼叫')
			uni.$emit('cancelCall', message)
			break;
		case '907':
			uni.$emit('transmitIce', message)
			console.log('同步webrtc的ice')
			console.log(message)
			break;
		case '908':
			uni.$emit('acceptOffer', message)
			console.log('收到主叫的webrtc的offer')
			break;
		case '909':
			uni.$emit('answer', message)
			console.log('收到被叫的webrtc的同步answer')
			break;
		case '910':
			if (!online) {
				globalData.tag = message.extra
				globalData.socket.bindRoomTag(message.extra)
				uni.navigateTo({
					url: '/pages/rtc/group?type=voiceds&message=' + encodeURIComponent(JSON.stringify(
						message)) + '&group=' +
						encodeURIComponent(message.content)
				})
				store.commit('user/SET_ONLINE', true)
			} else {
				API_rtc.busyCalls({
					tag: message.extra
				})
			}
			break;
		case '911':
			if (!online) {
				globalData.tag = message.extra
				globalData.socket.bindRoomTag(message.extra)
				uni.navigateTo({
					url: '/pages/rtc/group?type=videods&message=' + encodeURIComponent(JSON.stringify(
						message)) + '&group=' +
						encodeURIComponent(message.content)
				})
				store.commit('user/SET_ONLINE', true)
			} else {
				API_rtc.busyCalls({
					tag: message.extra
				})
			}
			break;
		case '912':
			uni.$emit('acceptRtc', message)
			console.log('对方接听了会议通话')
			break;
		case '913':
			console.log('对话群拒绝了通话')
			uni.$emit('rejectCall', message)
			break;
		case '914':
			console.log('对方设备正忙')
			uni.$emit('busyCall', message)
			break;
		case '915':
			console.log('音频状态')
			uni.$emit('voiceSwitch', message)
			break;
		case '916':
			console.log('摄像头状态')
			uni.$emit('cameraSwitch', message)
			break;
		case '917':
			console.log('添加人员')
			uni.$emit('addGrouped', message)
			break;
		case '918':
			uni.$emit('quitRtc', message)
			console.log('对方群退出房间')
			break;
		case '919':
			uni.$emit('cancelCall', message)
			console.log('对方群取消呼叫')
			break;
		case '920':
			uni.$emit('acceptOffer', message)
			console.log('收到会议offer')
			break;
		case '921':
			uni.$emit('answer', message)
			console.log('收到会议answer')
			break;
		case '922':
			uni.$emit('transmitIce', message)
			console.log('收到会议ice')
			break;
		case '923':
			uni.$emit('timeOut', message)
			console.log('会议无响应')
			break;
		case '924':
			console.log('对话群挂断解散了通话')
			uni.$emit('handleUp', message)
			break;
		case '925':
			console.log('全员禁言')
			uni.$emit('voiceDisable', message)
			break;
		case '926':
			console.log('解除禁言')
			uni.$emit('voiceResume', message)
			break;
		case '927':
			console.log('重新呼叫')
			uni.$emit('groupRecall', message)
			break;
		case '928':
			console.log('其他设备接听')
			uni.$emit('otherAccept', message)
			break;
		case '929':
			console.log('其他设备拒绝')
			uni.$emit('otherReject', message)
			break;
		case '930':
			console.log('群其他设备接听')
			uni.$emit('groupOtherAccept', message)
			break;
		case '931':
			console.log('群其他设备拒绝')
			uni.$emit('groupOtherReject', message)
			break;
	}
}