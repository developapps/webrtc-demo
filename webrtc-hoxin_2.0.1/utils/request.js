import Request from '@/utils/luch-request/index'
import Cache, {
  Keys
} from '@/utils/cache';
import configs from '@/config/config'
import store from '@/store'
// 初始化请求配置
const http = new Request();
http.setConfig((defaultConfig) => {
  /* defaultConfig 为默认全局配置 */
  defaultConfig.baseURL = configs.baseUrl /* 根域名 */
  return defaultConfig
})
class Ajax {
  ajax(options = {}) {
    if (options.isFile) {
      return http.upload(options.url, {
        // #ifdef MP-ALIPAY || MP-WEIXIN
        timeout: 30000, // 仅微信小程序（2.10.0）、支付宝小程序支持
        // #endif
        filePath: options.filePath,
        data: options.data,
        file: options.file,
        name: options.name,
        header: options.header,
        formData: options.formData,
        // 注：如果局部custom与全局custom有同名属性，则后面的属性会覆盖前面的属性，相当于Object.assign(全局，局部)
        custom: {
          isJson: options.isJson,
          needToken: options.needToken,
          token: options.token
        }, // 自定义参数
        getTask: (task, progressOptions) => {
          task.onProgressUpdate((res) => {
            // 测试条件，取消上传任务。
            // if (res.progress > 50) {
            //   uploadTask.abort();
            // }
            if (options.progressBack) {
              // console.log('上传进度' + res.progress);
              // console.log('已经上传的数据长度' + res.totalBytesSent);
              // console.log('预期需要上传的数据总长度' + res.totalBytesExpectedToSend);
              options.progressBack(res)
            }
          });
        },
      })
    } else {
      return http.request({
        method: options.method, // 请求方法必须大写
        url: options.url,
        data: options.data,
        // #ifdef MP-ALIPAY || MP-WEIXIN
        timeout: 30000, // 仅微信小程序（2.10.0）、支付宝小程序支持
        // #endif
        params: options.params,
        header: options.header,
        // 注：如果局部custom与全局custom有同名属性，则后面的属性会覆盖前面的属性，相当于Object.assign(全局，局部)
        custom: {
          isJson: options.isJson,
          needToken: options.needToken,
          token: options.token
        }, // 自定义参数
      })
    }
  }
}
let appType = 0
http.interceptors.request.use(async (config) => { // 可使用async await 做异步操作
    // 初始化请求拦截器时，会执行此方法，此时data为undefined，赋予默认{}
    // console.log(config)
    config.data = config.data || {}
    // 是否需要设置 token
    // console.log(config)
    //如果需要json传递方式
    // #ifdef H5
    appType = 7
    // #endif
    // #ifndef H5
    let platform = uni.getSystemInfoSync().platform
    if (platform == 'ios') {
      appType = 6
    } else if (platform == 'android') {
      appType = 5
    }
    // #endif
    if (config.custom.isJson) {
      config.header = {
        ...config.header,
        'x-app-language': 'zh-CN',
        'x-app-type': appType,
        'x-app-version': 1.0,
        'Content-Type': 'application/json',
      }
    } else {
      config.header = {
        ...config.header,
        'x-app-language': 'zh-CN',
        'x-app-type': appType,
        'x-app-version': 1.0,
        'Content-Type': 'application/x-www-form-urlencoded',
      }
    }
    if (config.custom.needToken) {
      //accessToken
      const accessToken = uni.getStorageSync('x-uid');
      //设置header信息
      config.header = {
        ...config.header,
        'x-app-language': 'zh-CN',
        'x-app-type': appType,
        'x-app-version': 1.0,
        'x-uid': accessToken,
      }
      if (config.custom.token) {
        config.header['x-uid'] = config.custom.token
      }
    }
    // 可以在此通过vm引用vuex中的变量，具体值在vm.$store.state中
    // console.log(vm.$store.state);
    return config
  }, (config) => // 可使用async await 做异步操作
  Promise.reject(config))
http.interceptors.response.use(async (response) => {
  /* 对响应成功做点什么 可使用async await 做异步操作*/
  return response.data || {}
}, (response) => {
  /*  对响应错误做点什么 （statusCode !== 200）*/
  const res = response.data
  if (res && res.code == 401) {
    console.log('如果是401，则清空登录信息');
    cleanAll(res.message);
    uni.showToast({
      title: res.message,
      icon: 'none',
      duration: 2000,
    })
  } else if (res && (res.code == 500 || res.code == 400)) {
    uni.showToast({
      title: 'code：' + res.code + ' msg：' + res.message,
      icon: 'none',
      duration: 2000,
    })
  } else {
    if (res && res.code && res.code !== 200) {
      uni.showToast({
        title: 'code：' + res.code + ' msg：' + res.message,
        icon: 'none',
        duration: 2000,
      })
    }
  }
  return response
})

//清空登录信息
function cleanAll() {
  const globalData = getApp().globalData
  store.dispatch('user/resetToken').then(() => {
    getApp().globalData.socket && globalData.socket.close()
    // #ifdef H5
    window.location.reload()
    // #endif
    // #ifndef H5
    uni.reLaunch({
      url: '/pages/auth/index'
    })
    // #endif
  })
}
export default new Ajax();

export const Method = {
  GET: 'GET',
  POST: 'POST',
  PUT: 'PUT',
  DELETE: 'DELETE',
};
