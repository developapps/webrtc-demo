import Vue from 'vue'
let vm = new Vue();
import UniSocket from "@/utils/uni.socket"
import store from '../store'
import checkCall from '@/utils/checkCall'
import checkNativeCall from '@/utils/checkNativeCall'
import config from '@/config/config'
const onLogout = () => {
  function cleanAll() {
    const globalData = getApp().globalData
    store.dispatch('user/resetToken').then(() => {
      uni.navigateTo({
        url: '/pages/auth/index'
      })
      globalData.socket.close()
    })
  }
  uni.showModal({
    title: '登录提示',
    content: '账号异地登录',
    success: function(res) {
      if (res.confirm) {
        cleanAll()
        console.log('退出登录操作')
      } else if (res.cancel) {
        console.log('用户点击取消');
        cleanAll()
      }
    }
  });

}
export function webSocket() {
  const globalData = getApp().globalData
  globalData.socket = null
  if (store.getters.user) {
    console.log('开始连接socket')
    const CIM_URI = config.cim_host;
    globalData.socket = new UniSocket({
      url: CIM_URI
    });

    const fz = []
	
	globalData.socket.on('connectioned', function() {
	  console.log('连接成功～～');
	  globalData.socket.bindAccount(store.getters.user.id)
	}, true)
    globalData.socket.on('*', async (message) => {
      // console.log('开始收到消息...',message)
      // .... 在此处理服务器发给你的邮件data     
      if (message.key === 'client_bind' && message.code === '200') {
        console.log("上线响应");
        return
      }
      const rexp = /^5\d{2}$/
      // 收消息
      console.log('收到的消息', message);
      if (message.action === '999') {
        onLogout()
        console.log('退出操作')
        return false
      }
      // 通话逻辑
      if (message.action >= 900) return vm.$nativeRtc.isUIKitEnable()?checkNativeCall(message): checkCall(message) 

      // 检测 是否有@我
    })
  }
}
