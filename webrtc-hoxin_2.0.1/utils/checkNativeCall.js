// 通话逻辑
// 900	单人语音通话请求
// 901	单人视频通话请求
// 902	对方接听了通话
// 903	对话拒绝了通话
// 904	对方设备正忙
// 905	对方挂断结束通话
// 906	对方取消呼叫
// 907	同步webrtc的ice
// 908	收到主叫的webrtc的offer
// 909	收到被叫的webrtc的answer
import Vue from 'vue'
let vm = new Vue();
import store from '@/store'
import * as API_rtc from '@/api/rtc'
import config from '@/config/config'

function handleMessage(message) {
	let newMessage = {
		"sender": message.sender,
		"action": message.action,
		"content": message.content,
		"extra": message.extra,
		"timestamp": message.timestamp
	}
	vm.$nativeRtc.onMessageReceived(newMessage)
}

function handleSingleReceived(message) {
	let newMessage = {
		"sender": message.sender,
		"action": message.action,
		"content": message.content,
		"extra": message.extra,
		"timestamp": message.timestamp
	}
	vm.$nativeRtc.onSingleIncomingCall(newMessage)
}

function handleRoomReceived(message) {
	let content = JSON.parse(message.content)
	let logo = {}
	Object.keys(content).forEach(id => {
		logo[id] = `${config.avatarUrl}/${id}.jpg`
	})
	let newMessage = {
		"member.logo": logo,
		"message.sender": message.sender,
		"message.action": message.action,
		"message.content": message.content,
		"message.extra": message.extra,
		"message.timestamp": message.timestamp
	}
	vm.$nativeRtc.onRoomIncomingCall(newMessage)
}
export default function checkNativeCall(message) {
	let globalData = getApp().globalData
	let online = store.getters.onLine
	const callId = store.getters.callId
	console.log('接收到语音通话请求', online)
	switch (message.action) {
		case '900':
			if (callId == message.sender) {
				store.commit('user/SET_ONLINE', false)
				online = store.getters.onLine
				uni.navigateBack()
			}
			if (!online) {
				store.commit('user/SET_ONLINE', true)
				store.commit('user/SET_CALLID', message.sender)
				// if (!uni.getStorageSync("appshowing")) return;
				handleSingleReceived(message)

			} else {
				API_rtc.busyCall({
					targetId: message.sender
				})
			}
			break;
		case '901':
			if (callId == message.sender) {
				store.commit('user/SET_ONLINE', false)
				online = store.getters.onLine
				uni.navigateBack()
			}
			if (!online) {
				store.commit('user/SET_ONLINE', true)
				store.commit('user/SET_CALLID', message.sender)
				// if (!uni.getStorageSync("appshowing")) return;
				handleSingleReceived(message)
			} else {
				API_rtc.busyCall({
					targetId: message.sender
				})
			}
			console.log('接收到视频通话请求')
			break;
		case '902':
			handleMessage(message)
			console.log('对方接听了通话');
			break;
		case '903':
			console.log('对话拒绝了通话')
			handleMessage(message)
			break;
		case '904':
			console.log('对方设备正忙')
			handleMessage(message)
			break;
		case '905':
			console.log('对方挂断结束通话')
			handleMessage(message)
			break;
		case '906':
			console.log('对方取消呼叫')
			handleMessage(message)
			store.commit('user/SET_ONLINE', false)
			break;
		case '907':
			handleMessage(message)
			console.log('同步webrtc的ice')
			break;
		case '908':
			handleMessage(message)
			console.log('收到主叫的webrtc的offer')
			break;
		case '909':
			handleMessage(message)
			console.log('收到被叫的webrtc的同步answer')
			break;
		case '910':
			if (!online) {
				// if (!uni.getStorageSync("appshowing")) return;
				globalData.tag = message.extra
				globalData.socket.bindRoomTag(message.extra)
				handleRoomReceived(message)
				store.commit('user/SET_ONLINE', true)
				store.commit('user/SET_CALLID', message.sender)
			} else {
				API_rtc.busyCalls({
					tag: message.extra
				})
			}
			break;
		case '911':
			if (!online) {
				// if (!uni.getStorageSync("appshowing")) return;
				globalData.tag = message.extra
				globalData.socket.bindRoomTag(message.extra)
				handleRoomReceived(message)
				store.commit('user/SET_ONLINE', true)
				store.commit('user/SET_CALLID', message.sender)
			} else {
				API_rtc.busyCalls({
					tag: message.extra
				})
			}
			break;
		case '912':
			handleMessage(message)
			console.log('对方接听了会议通话')
			break;
		case '913':
			console.log('对话群拒绝了通话')
			handleMessage(message)
			break;
		case '914':
			console.log('对方设备正忙')
			handleMessage(message)
			break;
		case '915':
			console.log('音频状态')
			handleMessage(message)
			break;
		case '916':
			console.log('摄像头状态')
			handleMessage(message)
			break;
		case '917':
			console.log('添加人员')
			handleMessage(message)
			break;
		case '918':
			handleMessage(message)
			console.log('对方群退出房间')
			break;
		case '919':
			handleMessage(message)
			console.log('对方群取消呼叫')
			break;
		case '920':
			handleMessage(message)
			console.log('收到会议offer')
			break;
		case '921':
			handleMessage(message)
			console.log('收到会议answer')
			break;
		case '922':
			handleMessage(message)
			console.log('收到会议ice')
			break;
		case '923':
			handleMessage(message)
			console.log('会议无响应')
			break;
		case '924':
			console.log('对话群挂断解散了通话')
			handleMessage(message)
			break;
		case '925':
			console.log('全员禁言')
			handleMessage(message)
			break;
		case '926':
			console.log('解除禁言')
			handleMessage(message)
			break;
		case '927':
			console.log('重新呼叫')
			handleMessage(message)
			break;
		case '928':
			console.log('其他设备接听')
			handleMessage(message)
			break;
		case '929':
			console.log('其他设备拒绝')
			handleMessage(message)
			break;
		case '930':
			console.log('群其他设备接听')
			handleMessage(message)
			break;
		case '931':
			console.log('群其他设备拒绝')
			handleMessage(message)
			break;
	}
}