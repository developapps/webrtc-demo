// import Cookies from "js-cookie";

const TokenKey = "accessToken";
const UserInfo = "user";
const num = 2; // 失效时间是几小时
const time = new Date(new Date().getTime() + num * 60 * 60 * 1000);
export function getToken() {
  return uni.getStorageSync(TokenKey);
}

export function setToken(token) {
  return uni.setStorageSync(TokenKey, token);
}

export function removeToken() {
  return uni.removeStorageSync(TokenKey);
}

export function getUserInfo() {
  return uni.getStorageSync(UserInfo);
}

export function setUserInfo(userInfo) {
  return uni.setStorageSync(UserInfo, userInfo);
}

export function removeUserInfo() {
  return uni.removeStorageSync(UserInfo);
}
