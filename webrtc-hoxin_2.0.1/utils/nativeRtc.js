// #ifdef APP-PLUS
const uniWfcUIKit = uni.requireNativePlugin("Hoxin-WebrtcPlugin");
console.log("Hoxin-WebrtcPlugin",uniWfcUIKit)
// #endif
// #ifdef H5
const uniWfcUIKit = null
// #endif
import config from '@/config/config'
import store from "@/store"
import Vue from 'vue'
let vm = new Vue();
export class WfcUIKit {
	constructor(opt) {
		this.CallSession = {
			"self.id": store.getters.user.id,
			"self.name": store.getters.user.name,
			"self.logo": '',
			"api.host": config.baseUrl,
			"ice.servers": config.iceServers
		}
	}
	/**
	 * 判断 UIKit 原生插件是否集成
	 * @return {boolean}
	 */
	isUIKitEnable() {
		return !!uniWfcUIKit;
	}
	//初始化配置
	setup() {
		this.CallSession['self.name'] = store.getters.user.name
		this.CallSession['self.id'] = store.getters.user.id
		this.CallSession['self.logo'] = config.avatarUrl + '/' + store.getters.user
			.id + '.jpg'
		uniWfcUIKit.setupAppConfig(this.CallSession);
		// #ifdef APP-PLUS
		let globalEvent = uni.requireNativePlugin('globalEvent');
		// #endif
		// #ifdef H5
		let globalEvent = {}
		// #endif
		let _this = this
		globalEvent.removeEventListener('HoxinWebrtcEvent')
		globalEvent.addEventListener('HoxinWebrtcEvent', function(e) {
			_this.handleListenNative(e)
		});
	}
	// 更新通讯录
	updateFriendList(data) {
		uniWfcUIKit.setupContactList(data);
	}
	/**
	 * 发起单人音频通话
	 * @param {string} userId 用户 id
	 * @param {boolean} audioOnly 是否是音频通话
	 */
	startSingleVoiceCall(data) {
		console.log(data)
		uniWfcUIKit.callSingleVoice(data);
	}
	/**
	 * 发起单人视频通话
	 * @param {string} userId 用户 id
	 * @param {boolean} audioOnly 是否是音频通话
	 */
	startSingleVideoCall(data) {
		uniWfcUIKit.callSingleVideo(data);
	}
	/**
	 * 发起群音频通话
	 * @param {string} groupId 群 id
	 * @param {[string]} participants 参与者 id，要求是群成员
	 * @param {boolean} audioOnly 是否是音频通话
	 */
	startMultiVoiceCall(members, maximumNumber = 9) {
		uniWfcUIKit.callRoomVoice({
			"maximumNumber": maximumNumber,
			members
		})
	}
	/**
	 * 发起群视频通话
	 * @param {string} groupId 群 id
	 * @param {[string]} participants 参与者 id，要求是群成员
	 * @param {boolean} audioOnly 是否是音频通话
	 */
	startMultiVideoCall(members, maximumNumber = 9) {
		uniWfcUIKit.callRoomVideo({
			"maximumNumber": maximumNumber,
			members
		})
	}
	/**
	 * 同步消息
	 * @param {string} groupId 群 id
	 * @param {[string]} participants 参与者 id，要求是群成员
	 * @param {boolean} audioOnly 是否是音频通话
	 */
	onMessageReceived(data) {
		console.log('接收到单/多音视频Ice事件')
		console.log(data)
		uniWfcUIKit.onWebrtcEventMessage(data)
	}
	onSingleIncomingCall(data) {
		console.log('收到单人拨打通话事件')
		console.log(data)
		uniWfcUIKit.onSingleIncomingCall(data)
	}
	onRoomIncomingCall(data) {
		console.log('收到多人拨打通话事件')
		console.log(data)
		uniWfcUIKit.onRoomIncomingCall(data)
	}
	// 重新拨打音频多人
	recallRoomVoice(idList, members, maximumNumber = 9) {
		console.log('重播音频')
		uniWfcUIKit.recallRoomVoice({
			maximumNumber,
			idList,
			members
		})
	}
	// 重新拨打视频多人
	recallRoomVideo(idList, members, maximumNumber = 9) {
		console.log('重播视频')
		uniWfcUIKit.recallRoomVideo({
			maximumNumber,
			idList,
			members
		})
	}
	// 重回会议
	reenterCallRoom(data) {
		uniWfcUIKit.reenterCallRoom(data)
	}
	// 通讯录新增联系人
	addContact(data) {
		uniWfcUIKit.addContact(data);
	}
	// 通讯录删除联系人
	removeContact(data) {
		uniWfcUIKit.removeContact(data);
	}
	handleListenNative(data) {
		let key = ''
		if (data.key === 'ACTION_CALL_FINISHED') { //单人通话完结
			if (data.data.state == 0) key = 'hangup'
			if (data.data.state == 1) key = 'cancelCall'
			if (data.data.state == 2) key = 'rejectCall'
			if (data.data.state == 3) key = 'busyCall'
			if (data.data.state == 4) key = ''
			if (data.data.state == 5) key = ''
			this.createMessage(data.data, key)
		}
		if (data.key === 'ACTION_ROOM_CALL_FINISHED') { // 多人通话完结事件
			this.createGroupMessage(data.data)
		}
		if (data.key === 'ACTION_CALL_SET_ROOM_TAG') {
			getApp().globalData.tag = data.data
			getApp().globalData.socket.bindRoomTag(data.data)
		}
		if (data.key === 'ACTION_START_ROOM_CALLING') {
			store.commit('user/SET_ONLINE', true)
		}
	}
	async createMessage(data, key) {
		store.commit('user/SET_ONLINE', false)
		store.commit('user/SET_CALLID', -1)
		// let type = data.type === 0 ? 'voice' : 'video'
		// let idfer = data.role
		// const message = {
		// 	id: new Date().getTime(),
		// 	sender: idfer === 0 ? store.getters.user.id : data.uid, //自己
		// 	receiver: idfer === 0 ? data.uid : store.getters.user.id,
		// 	action: '',
		// 	content: '',
		// 	extra: '',
		// 	format: 0,
		// 	loading: 0,
		// 	isLocal: 1,
		// 	fail: 0,
		// 	unread: 0,
		// 	createTime: new Date().getTime()
		// }
		// if (key === 'hangup') {
		// 	message.content = JSON.stringify({
		// 		text: '通话结束',
		// 		type: type
		// 	})
		// 	message.callTime = this.showNum(parseInt(data.duration / 1000 / 60 / 60)) + ':' + this.showNum(parseInt(
		// 		data.duration / 1000 /
		// 		60) % 60) + ':' + this.showNum(parseInt(
		// 		data.duration / 1000 % 60))
		// 	message.action = '905'
		// }
		// if (key === 'rejectCall') {
		// 	message.content = !idfer ? JSON.stringify({
		// 		text: '已拒绝',
		// 		type: type
		// 	}) : JSON.stringify({
		// 		text: '对方已拒绝',
		// 		type: type
		// 	})
		// 	message.callTime = ''
		// 	message.action = '903'
		// }
		// if (key === 'cancelCall') {
		// 	message.content = !idfer ? JSON.stringify({
		// 		text: '已取消',
		// 		type: type
		// 	}) : JSON.stringify({
		// 		text: '对方已取消',
		// 		type: type
		// 	})
		// 	message.callTime = ''
		// 	message.action = '906'
		// }
		// if (key === 'busyCall') {
		// 	message.content = JSON.stringify({
		// 		text: '对方正忙',
		// 		type: type
		// 	})
		// 	message.callTime = ''
		// 	message.action = '904'
		// }
		// if(key === '') {
		// 	return
		// }
		// const handleMsg = await assembleMessage(message, idfer == 0 ? 1 : 0)
		// const msgId = await insertMessage(handleMsg)
		// console.log(msgId)
		// await isReply(message, idfer == 0 ? message.receiver : message.sender, idfer == 0 ? message.sender : message
		// 	.receiver)
		// await updateReply()
		// uni.$emit('updateMessageList', msgId, data.uid)
	}
	showNum(num) {
		if (num < 10) {
			return '0' + num
		}
		return num
	}
	async createGroupMessage(data) {
		// let globalData = getApp().globalData
		// const message = {
		// 	receiver: store.getters.user.id,
		// 	sender: data.uid,
		// 	content: '',
		// 	memberList: data.member,
		// 	action: '',
		// 	format: 0,
		// 	callTime: '',
		// 	video: 0,
		// 	audio: 0,
		// 	unread: 0,
		// 	extra: data.tag,
		// 	createTime: new Date().getTime()
		// }
		// if (data.type === 11) {
		// 	message.video = true
		// }
		// if (data.type === 10) {
		// 	message.audio = true
		// }
		// let callList = store.getters.callList;
		// if (data.state == 0) {
		// 	message.content = '已接通'
		// 	message.callTime = this.showNum(parseInt(data.duration / 1000 / 60 / 60)) + ':' + this.showNum(parseInt(
		// 		data.duration / 1000 /
		// 		60) % 60) + ':' + this.showNum(parseInt(
		// 		data.duration / 1000 % 60))
		// 	message.action = '912'
		// }
		// if (data.state == 1) {
		// 	message.action = '919'
		// 	message.content = '已取消'
		// 	message.unread = 1

		// }
		// if (data.state == 2) {
		// 	message.content = '已拒绝'
		// 	message.callTime = ''
		// 	message.action = '913'
		// }
		// if(data.state == 4 || data.state == 5) {
		// 	return
		// }
		store.commit('user/SET_ONLINE', false)
		globalData.tag = null
		globalData.socket.bindRoomTag()
		// await inserCall(message)
		// uni.$emit('callInited')
	}
}

const self = new WfcUIKit();
export default self;