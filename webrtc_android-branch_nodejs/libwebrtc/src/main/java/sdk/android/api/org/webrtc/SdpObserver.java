/*
 *  Copyright 2013 The WebRTC project authors. All Rights Reserved.
 *
 *  Use of this source code is governed by a BSD-style license
 *  that can be found in the LICENSE file in the root of the source
 *  tree. An additional intellectual property rights grant can be found
 *  in the file PATENTS.  All contributing project authors may
 *  be found in the AUTHORS file in the root of the source tree.
 */

package org.webrtc;

/**
 * 接口用于观察sdp相关事件。
 * 在.so 里回调出来
 */
public interface SdpObserver {
    /**
     * 在Create{Offer,Answer}()成功时调用。
     */
    @CalledByNative
    void onCreateSuccess(SessionDescription sdp);

    /**
     * 成功调用Set{Local,Remote}Description()。
     */
    @CalledByNative
    void onSetSuccess();

    /**
     * 在Create{Offer,Answer}()错误时调用。
     */
    @CalledByNative
    void onCreateFailure(String error);

    /**
     * 在Set{Local,Remote}Description()出错时调用。
     */
    @CalledByNative
    void onSetFailure(String error);
}
